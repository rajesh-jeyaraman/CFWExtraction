package com.p3.archon.schemagenerator;

public class SchemaConstant {

	public static final String ROOT_TAG = "<xs:schema attributeFormDefault=\"unqualified\" elementFormDefault=\"qualified\" targetNamespace=\"::::: SCHEMA :::::\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\">\n" + 
			"  <xs:element name=\"RECORDs\">\n" + 
			"    <xs:complexType>\n" + 
			"      <xs:sequence>\n" + 
			"        <xs:element maxOccurs=\"unbounded\" minOccurs=\"0\" name=\"RECORD\">\n" + 
			"          <xs:complexType>\n" + 
			"            <xs:sequence>";
	
	public static final String ROOT_END_TAG = "</xs:sequence>\n" + 
			"          </xs:complexType>\n" + 
			"        </xs:element>\n" + 
			"      </xs:sequence>\n" + 
			"    </xs:complexType>\n" + 
			"  </xs:element>\n" + 
			"</xs:schema>";
	
	public static final String PARENT_START_TAG = "<xs:element name=\"::::: PARENT_TAG_NAME :::::\" ::::: OCCURENCE :::::>\n" + 
			"                <xs:complexType>\n" + 
			"                  <xs:sequence>\n";
	
	public static final String PARENT_END_TAG = "</xs:sequence>\n" + 
			"                            </xs:complexType>\n" + 
			"                          </xs:element>\n";
	
	public static final String PARENT_TAG_NAME = "::::: PARENT_TAG_NAME :::::";
	public static final String OCCURENCE = "::::: OCCURENCE :::::";

	public static final String CHILD_TAG = "<xs:element type=\"::::: DATA_TYPE :::::\" name=\"::::: CHILD_TAG_NAME :::::\" ::::: OCCURENCE :::::/>\n";
	public static final String DATA_TYPE_TAG = "::::: DATA_TYPE :::::";
	public static final String CHILD_TAG_NAME = "::::: CHILD_TAG_NAME :::::";
	
	
	public static final String ATTACHMENT_TAG_INFO = " <xs:element  name=\"Attachments\">\n" + 
			"                            <xs:complexType>\n" + 
			"                              <xs:sequence>\n" + 
			"                                <xs:element name=\"Attachment\" maxOccurs=\"unbounded\" minOccurs=\"0\">\n" + 
			"								 	<xs:complexType>\n" +
			"										<xs:sequence>\n" +
			"											<xs:element name=\"AttachmentName\">\n" +
			"												<xs:simpleType>\n" +
			"													<xs:restriction base=\"xs:normalizedString\" >\n"+
			"														<xs:minLength value=\"1\" />\n" +
			"														<xs:maxLength value=\"512\" />\n" +
			"													</xs:restriction>\n" +
			"												</xs:simpleType>\n" +	
			"											</xs:element>\n" +
			"											<xs:element name=\"FileName\" >\n" +
			"												<xs:simpleType>\n" +
			"													<xs:restriction base=\"xs:normalizedString\" >\n"+
			"														<xs:minLength value=\"1\" />\n" +
			"														<xs:maxLength value=\"512\" />\n" +
			"													</xs:restriction>\n" +
			"												</xs:simpleType>\n" +	
			"											</xs:element>\n" +	
			"											<xs:element name=\"CreatedBy\" >\n" +
			"												<xs:simpleType>\n" +
			"													<xs:restriction base=\"xs:normalizedString\" >\n"+
			"														<xs:minLength value=\"1\" />\n" +
			"														<xs:maxLength value=\"32\" />\n" +
			"													</xs:restriction>\n" +
			"												</xs:simpleType>\n" +	
			"											</xs:element>\n" +	
			"											<xs:element name=\"CreatedOnDate\" type=\"xs:dateTime\" />\n" +
			"										</xs:sequence>\n" +
			"									</xs:complexType>\n" +
			"								 </xs:element>\n" +		
			"                              </xs:sequence>\n" + 
			"                            </xs:complexType>\n" + 
			"                          </xs:element>";

	public static final String SCHEMA = "::::: SCHEMA :::::";
	
	
}
