package com.p3.archon.schemagenerator;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Stack;

import com.p3.archon.jsonparser.JsonProcessor;
import com.p3.archon.jsonparser.bean.FinalChildren;

public class SchemaGenerator {

	private Writer out;

	public void begin(String sipOutputFolder, String configFileName, String schema) throws IOException {
		sipOutputFolder += File.separator + "pdi-schema_" + new File(configFileName).getName() + ".xsd";
		out = new OutputStreamWriter(new FileOutputStream(new File(sipOutputFolder)));
		Stack<String> taginfo = new Stack<String>();
		String[] currentPath = null;
		String tagName = "";
		int childLevel = 0;

		out.write(SchemaConstant.ROOT_TAG.replace(SchemaConstant.SCHEMA, schema));
		for (FinalChildren child : JsonProcessor.resultList) {
			// System.out.println(child.getTopath());
			currentPath = child.getTopath().startsWith("/") ? child.getTopath().substring(1).split("/")
					: child.getTopath().split("/");
			checkCloseTags(taginfo, currentPath);
			for (int i = 0; i < currentPath.length; i++) {
				tagName = currentPath[i];
				childLevel = currentPath.length - 1;

				if (i == childLevel) {
					// System.out.println(taginfo + " " + tagName);
					out.write(SchemaConstant.CHILD_TAG
							.replace(SchemaConstant.DATA_TYPE_TAG, getValidDataType(child.getDatatype()))
							.replace(SchemaConstant.CHILD_TAG_NAME, child.getName())
							.replace(SchemaConstant.OCCURENCE, i > 0 ? "minOccurs=\"0\"" : ""));
				} else if (taginfo.isEmpty() || i > taginfo.size() - 1) {
					taginfo.push(tagName);
					// System.out.println(taginfo);
					out.write(SchemaConstant.PARENT_START_TAG.replace(SchemaConstant.PARENT_TAG_NAME, currentPath[i])
							.replace(SchemaConstant.OCCURENCE,
									(i > 1 && i == childLevel - 1) ? "maxOccurs=\"unbounded\" minOccurs=\"0\""
											: "minOccurs=\"0\""));
				}

			}

		}
		closeRemainingTag(taginfo);
		out.write(SchemaConstant.ROOT_END_TAG);
		out.flush();
		out.close();
		System.out.println("Pdi Schema generated at location " + sipOutputFolder);
	}

	private void checkCloseTags(Stack<String> taginfo, String[] currentPath) throws IOException {
		for (int i = 0; i < taginfo.size(); i++) {
			if (currentPath.length > i && !taginfo.get(i).equals(currentPath[i])) {
				closeTags(taginfo, i);
				break;
			}
		}
	}

	private CharSequence getValidDataType(String datatype) {
		switch (datatype.toUpperCase()) {
		case "STRING":
			return "xs:string";
		case "INTEGER":
		case "INT":
		case "LONG":
			return "xs:long"; // SINCE the output_workflow json return only int and not long, if long is
								// provided by output_workflow_json, then add a separete case for long and
								// change this to int
		case "DATETIME":
			return "xs:dateTime";
		case "DATE":
			return "xs:date";
		case "TIME":
			return "xs:time";
		default:
			return "xs:string";
		}
	}

	private void closeRemainingTag(Stack<String> taginfo) throws IOException {
		final int tagSize = taginfo.size();
		for (int i = 0; i < tagSize; i++) {
			if (i == tagSize - 1) {
				// print attachement schema
				out.write(SchemaConstant.ATTACHMENT_TAG_INFO);
			}
			out.write(SchemaConstant.PARENT_END_TAG);
			taginfo.pop();
		}
	}

	private void closeTags(Stack<String> taginfo, int i) throws IOException {
		final int tagSize = taginfo.size();
		for (int j = i; j < tagSize; j++) {
			taginfo.pop();
			out.write(SchemaConstant.PARENT_END_TAG);
			// System.out.println(taginfo);
		}
	}
}
